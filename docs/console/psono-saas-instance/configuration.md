---
title: Configuration
metaTitle: Configuration of a Psono SaaS instance | Psono Documentation
meta:
  - name: description
    content: Instructions how to configure a Psono SaaS instance
---

# Configuration

## Preamble

We assume that you already have provisioned a Psono SaaS instance, if not please create one.

## Guide

1)  Go to "Psono SaaS" and click the pencil icon button

    ![Step 1 Go to "Psono SaaS" and click the "+"](/images/console/psono-saas-instance/creation-psono-saas-instance-edit.png)



2)  Setup custom domain (optional)

    You may want to use an own domain, e.g. psono.corp.com to access your instance. It is best to configure the custom
    domain at the beginning before you setup, e.g. SAML or onboard users. Click on "Custom Domain"
    
    ![Step 2 Click pencil symbol to add custom domain](/images/console/psono-saas-instance/configuration-psono-saas-instance-custom-domain.png)

    Enter your preferred domain, e.g. `psono.corp.com`
    
    ![Step 2 Enter custom domain](/images/console/psono-saas-instance/configuration-psono-saas-instance-custom-domain-2.png)

    Configure the displayed `CNAME` and `TXT` type DNS records.
    
    ![Step 2 Configure domain records](/images/console/psono-saas-instance/configuration-psono-saas-instance-custom-domain-3.png)

    Use the "arrow" button at the top to go back to the regular settings.

    ::: tip
    It may take a couple of minutes for those domain records to show up.
    :::

3)  Configure `settings.yaml`

    You can now modify all the settings according to your requirements. The settings look "cryptic" yet "map" to the
    settings of the regular settings of the regular `settings.yaml`. Details about each can be found in the regular
    Admin documentation here e.g. [/admin/overview/summary.html](/admin/overview/summary.html).
    
    ![Step 3 Modify settings.yaml](/images/console/psono-saas-instance/configuration-psono-saas-instance-settings.png)

4)  Configure `config.json`

    Some of the documents require that you modify a `config.json`. There are two of those `config.json`, one for the regular
    webclient and one for the portal. You will find these settings at the end as shown in the screenshot below.
    
    ![Step 4 Modify the config.json](/images/console/psono-saas-instance/configuration-psono-saas-instance-config-json.png)



