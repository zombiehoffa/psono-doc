---
title: Overview
metaTitle: Getting Started Overview | Psono Documentation
meta:
  - name: description
    content: These brief instructions will help you get started quickly with the Psono password manager. The other topics in this help provide additional information and detail about working with other aspects.
---

# Getting Started Overview

The Psono password manager consists of two components, a client and a server:

1.  Client

    The client is usually the thing that you "start" to access your data that is stored on a server.
    It's your main working utility and it comes (currently) in four flavors.

    a) As a web site, that your administrator will setup for you. A demo of the web site can be found on [psono.pw](https://www.psono.pw)

    b) As a [Firefox Extension](https://addons.mozilla.org/firefox/addon/psono-pw-password-manager/)

    c) As a [Chrome Extension](https://chrome.google.com/webstore/detail/psonopw-password-manager/eljmjmgjkbmpmfljlmklcfineebidmlo)

    d) As an [Android App](https://play.google.com/store/apps/details?id=com.psono.psono)
    
    e) As an [iOS App](https://apps.apple.com/us/app/psono-password-manager/id1545581224)


2.  Server

    The server is the "core" of the system, doing all the logic, and stores your data. Before you register / login you can point your
    client to use a different server, but that's the only "interaction" that you have.


![Overview of a typical setup](/images/user_setup.png)

::: tip
All Secrets (Passwords, Notes, Bookmarks, ...) are encrypted on your PC in your browser, before they are stored on the server.
:::

