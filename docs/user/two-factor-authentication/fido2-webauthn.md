---
title: Fido2 / Webauthn
metaTitle: Setup Fido2 / Webauthn two-factor-authentication | Psono Documentation
meta:
- name: description
  content: How to protect your account with Fido2 / Webauthn two-factor-authentication
---

# Setup Fido2 / Webauthn two-factor-authentication

## Preamble

Another well known two-factor-authenticator is Fido2 / Webauthn. We strongly recommend setting up such a 2-Factor Authentication to
protect your account. Please generate a recovery code afterwards so you always have a possibility to gain access to your account in case
you lose your device.

::: warning
The webauthn's secret are stored on the device and are only available in the security context that you used to create them.
So you need another second factor or recovery codes to login to other devices / clients / apps / extension / ... or risk losing
access to your account.
:::

## Pre-requirements

You need a device that supports Fido2 / Webauthn and an up to date browser. All modern devices have a TPM / T2 chip and should
support WebAuthn. Psono also supports external devices like for example Yubikeys for WebAuthn.

## Setup Guide


1)  Login into your account:

![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](/images/user/basic/step2-go-to-account.jpg)

3)  Select the “Multifactor Authentication” tab:

![Step 3 go to multifactor authentication](/images/user/2fa_webauthn/step3-go-to-multifactor-authentication.jpg)

4)  Click the "Configure" button next to "Fido2 / Webauthn":

![Step 4 click configure next to Fido2 / Webauthn](/images/user/2fa_webauthn/step4-click-configure-next-to-webauthn.jpg)

5)  Click the little plus:

![Step 5 click little plus](/images/user/2fa_webauthn/step5-click-on-plus.jpg)

6) Enter title:

Type some descriptive title that helps you identify the device and click generate.

![Step 6 add details](/images/user/2fa_webauthn/step6-some-descriptive-title.jpg)

You should be asked now by your browser which device you want to use for second factor.
We strongly recommend to set up a recovery code in case your device gets damaged, is lost or stolen.


