---
title: Autofill with extensions
metaTitle: Autofill with extensions | Psono Documentation
meta:
  - name: description
    content: How does Psono's autofill with browser extension work?
---

# Autofill with extensions

## Preamble

For convenience Psono's extensions offers the possibility to autofill password forms. This will explain how the mechanism works.

## How does autofill with extensions work?

1) Make sure that you installed the browser extension and that you are logged in. Afterwards the process looks like this:

2) When you Edit an entry and open the "advanced" options (Link at the bottom right), you will see something similar to this:

![Config for Autofill with extensions](/images/user/autofill_extensions/psono-config.png)

3) So when the entry was created you specified an URL (second field from the top, e.g. https://example.com/users/sign_in).

4) The extension automatically "extracted" a domain for the so called "domain filter" (fourth field from the bottom, e.g. example.com).

::: tip
You can manually modify this urlfilter and also add multiple other domains, separated by a space.
:::

5) Whenever you visit now a page, that has a password form, Psono will list all entries with matching urlfilters.

![Password capture notification](/images/user/autofill_extensions/loginform_opend_selection.png)

6) Click on the entry and Psono will automatically fill out the username and password.

![Password capture notification](/images/user/autofill_extensions/loginform_filled.png)