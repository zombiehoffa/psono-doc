---
title: Delete Account
metaTitle: Delete Account | Psono Documentation
meta:
  - name: description
    content: How to delete an account
---

# Delete Account

This guide explains how to delete your own account. Data shared with other people will be unaffected and will not be deleted.

1)  Login into your account:
    
    ![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Go to "Account":
    
    ![Step 2 go to account](/images/user/basic/step2-go-to-account.jpg)

3)  Select the “Delete Account” tab:
    
    ![Step 3 Select Delete Account](/images/user/delete_account/step-3-go-to-delete-account.jpg)

4)  Click "Delete"

5)  Confirm account delete:

    Enter your password and click on "Delete" to confirm that your account should be deleted.
    
    ![Step 5 Go to the "new Emergency Codes" tab](/images/user/delete_account/step-5-confirm-delete-account.jpg)

    ::: warning
    Accounts that have been deleted cannot be restored.
    :::