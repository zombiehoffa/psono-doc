---
title: Creation
metaTitle: Creation of the API key | Psono Documentation
meta:
  - name: description
    content: Instructions how to create the API key.
---

# Creation

## Preamble

In order to create an API key you need to have an account and be logged in with a browser client.

## Guide

1)  Go to to "other":

![Step 1 Go to Other](/images/user/api_key_creation/step1-go-to-other.jpg)

2) Click the plus symbol:

![Step 2 Click plus symbol](/images/user/api_key_creation/step2-create-new-api-key-by-clicking-plus.jpg)

3)  Configure your API key:

Next to the title you can specify secrets that you want to make accessible with this api key, specify if you want to
"Allow insecure usage" (e.g. usage with remote decryption) or "secret restriction" (e.g. usage with sessions)

![Step 3 Configure your API key](/images/user/api_key_creation/step3-specify-title-settings-and-add-secrets.jpg)

4)  Edit the created API key:

After clicking save click on the wrench symbol to edit the entry and display the actual secret key properties.

![Step 4 Edit the created API key](/images/user/api_key_creation/step4-edit-the-created-api-key.jpg)

5)  Copy details:

Copy the shown details.

![Step 2 Copy details](/images/user/api_key_creation/step5-copy-details.jpg)

6)  (optional) Add secrets:

If you are using the API key without session you need to add secrets:

![Step 6 Add secrets](/images/user/api_key_creation/step6-add-secrets.jpg)

A `SECRET_ID` will be shown for each secret. You will need that later too.
