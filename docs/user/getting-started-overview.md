# Overview

The Psono password manager consists of two components, the client and the server:

1.  Client

    The client comes is usually the thing that you "start" and accesses your data that is stored on the server.
    Its your main working utility and it comes (currently) in three flavors.

    a) As a web site, that your administrator will setup for you. A demo of the web site can be found on [psono.pw](https://www.psono.pw)

    b) As a [Firefox extension](https://addons.mozilla.org/firefox/addon/psono-pw-password-manager/)

    c) As a [Chrome extension](https://chrome.google.com/webstore/detail/psonopw-password-manager/eljmjmgjkbmpmfljlmklcfineebidmlo)


2.  Server

    The server is the "core" of the system, doing all the logic, and stores your data. Before you register / login you can point your
    client to use a different server, but thats the only "interaction" that you have.


![Overview of a typical setup](images/user_setup.png)

::: tip
All Secrets (Passwords, Notes, Bookmarks, ...) are encrypted on your PC in your browser, before they are stored on the server.
:::


{% include links.html %}