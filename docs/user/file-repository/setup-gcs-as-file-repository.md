---
title: Setup GCS as file repository
metaTitle: Setup GCS as file repository | Psono Documentation
meta:
  - name: description
    content: Instructions how to setup a Google Cloud Storage bucket as file repository
---

# Setup GCS as file repository

Instructions how to setup a Google Cloud Storage bucket as file repository


## Pre-requirements

You need to have an Google Cloud Platform account. If not, you can register here [cloud.google.com](https://cloud.google.com/)
As a new customer Google will provide you with 300 USD for the first 12 months and a lot of other benefits forever free,
e.g. 5 GB of Google Cloud Storage. More details can be found here: [cloud.google.com/free/](https://cloud.google.com/free/)


## Setup Guide

This guide will walk you through the creation of a bucket, the configuration of the bucket and the creation of a
service account, before it helps you to configure it in psono.

### Create bucket

1) Login to cloud.google.com

2) Create a project at the top

3) Go to Storage -> Browser

![Step 3 Go to storage](/images/user/file_repository_setup_gcs/step3-storage-browser.jpg)

4) Click "Create bucket"

![Step 4 Create bucket](/images/user/file_repository_setup_gcs/step4-create-bucket.jpg)

5) Click "Setup bucket"

Specify a name, a region and click "Create".

::: tip
Remember the name that you specified as you will need it later.
:::

![Step 5 Setup bucket](/images/user/file_repository_setup_gcs/step5-setup-bucket.jpg)

::: tip
Remember the bucket name. You will need it later.
:::

### Configure CORS

6) Click at the top to open a "Cloud Shell"

![Step 6 Open Cloud Shell](/images/user/file_repository_setup_gcs/step6-open-cloud-shell.jpg)

7) Copy & paste the following two commands

```bash
cat <<EOT >> gcs_cors.json
[
    {
      "origin": ["*"],
      "responseHeader": ["content-type", "cache-control", "if-modified-since", "pragma"],
      "method": ["*"],
      "maxAgeSeconds": 5
    }
]
EOT

gsutil cors set gcs_cors.json gs://psono-file-upload
```

::: tip
Replace 'psono-file-upload' with your bucket name.
:::

![Step 6 Open Cloud Shell](/images/user/file_repository_setup_gcs/step7-copy-paste-cors-config.jpg)


### Create service account

8) Go to IAM & admin > Service accounts

![Step 8 Go to IAM & admin service accounts](/images/user/file_repository_setup_gcs/step8-go-to-iam-admin-service-accounts.jpg)

9) Click "Create service account"

![Step 9 click "Create service account"](/images/user/file_repository_setup_gcs/step9-click-create-service-account.jpg)

10) Specify the service account name

![Step 10 specify the service account name](/images/user/file_repository_setup_gcs/step10-spcify-service-account-name.jpg)

11) Configure the role and grant "Storage Object Admin"

![Step 11 Configure the role as storage object admin](/images/user/file_repository_setup_gcs/step11-configure-role.jpg)

12) Click "Create Key" and select JSON

![Step 12 Create the key as JSON](/images/user/file_repository_setup_gcs/step12-create-key.jpg)


### Configure the file repository


13) Login to Psono

![Step 13 Login to Psono](/images/user/file_repository_setup_gcs/step13-login-to-psono.jpg)

14) Go to "Other"

![Step 14 Go to other](/images/user/file_repository_setup_gcs/step14-go-to-other.jpg)

15) Go to "File Repositories" and click "Create new file repository"

![Step 15 Go to "File Repositories" and click "Create new file repository"](/images/user/file_repository_setup_gcs/step15-select-file-repository-and-click-create-new-file-repository.jpg)

16) Configure the file repository

Use any descriptive title, select GCP Cloud Storage as type, add your buckets name and copy paste the content of the JSON Key.

![Step 16 Configure the file repository](/images/user/file_repository_setup_gcs/step16-configure-repository.jpg)

You can now upload files from the datastore to this file repository.


