---
title: Supported Features
metaTitle: Admin Documentation of Psono | Psono Documentation
meta:
  - name: description
    content: These brief instructions will help you get started quickly with the Psono password server. The other topics in this help provide additional information and detail about working with other aspects.
---

# Supported Features

Before you get into exploring Psono as a potential password manager, you may be wondering if it supports some basic
features needed to fulfill your requirements. The following table shows what is supported in Psono.

If you are looking more for features for users, then you can find them here in our [Features for Users](/user/getting-started/features.html)

## Features for administrators

Features | Supported | Notes
---------|-----------|-------
Healthcheck | Yes | An API endpoint to monitor the health of your installation, including DB connectivity, time sync and various other
Prevent old passwords | Yes | Old passwords can be blocked without compromises for the "never send the password to the server"
Restrict 2FA methods | Yes | If administrators only want to allow particular 2FA methods, then they can restrict them.
Search by email address | Yes | By default the server does not allow to search users by their email address, but in a corporate environment administrators can enable this.
Search by partial usernames | Yes | By default the server does not allow to search users by parts of their username, but in a corporate environment administrators can enable this.
Admin dashboard | Yes | Psono offers with the "Admin Client" a nice web based dashboard to manage users and their accounts.
Console commands | Yes | Allows to automated processes like user generation or promotion of users with scripts.
Broad OS support | Yes | Psono can run on any system that can provide a docker environment, e.g. AWS, Azure, GCE, Mac, Windows, Debian, Ubuntu, CentOS, Fedora, RHEL, SUSE, Oracle Linux
Vertical scalability | Yes | Psono architecture allows vertical scalability and is only limited by the amount of write operations a single Postgres can handle (which is a lot with the right hardware).
High availability | Yes | Psono architecture allows a full redundant setup for secrets and filestorage.
Site affine filestorage | Yes | Allowing setups with dedicated storage for remote offices, or "external" users.
Multiple storage backends | Yes | The fileserver supports various storage provider (local storage, Google Cloud Storage, AWS, Azure, ...) for client side encrypted files
Central security reports | Yes | Central security reports you to audit the passwords of your users, check if they have been breached and if they follow password policies with length or complexity requirements without access to the actual passwords.

## Features exclusively in the Enterprise Edition

Features | Supported | Notes
---------|-----------|-------
LDAP Authentication* | Yes | Allows to login with their LDAP credentials
SAML Authentication** | Yes | Allows to login with SAML SSO
OIDC Authentication** | Yes | Allows to login with OpenId Connect SSO
LDAP Groups* | Yes | Adds users automatically groups based on their LDAP groups
SAML Groups** | Yes | Adds users automatically groups based on their SAML groups
OIDC Groups** | Yes | Adds users automatically groups based on their OpenId Connect groups
Audit Logging | Yes | Once enabled the server will log all events. Administrators can control what they are interested with white and blacklists.
2FA Enforcement | Yes | Users can be forced to provide a second factor before they can use Psono.
Disable Export | Yes | Users can be denied the possibility to export their passwords.
Disable API Keys | Yes | API keys bypass external authentication and therefore can be disabled if required.
Disable Emergency Codes | Yes | Emergency codes can violate company policies, as they bypass external authentication
Disable Recovery Codes | Yes | Recovery codes can be disabled if policies require or external authentication providers are in place
Disable File Repositories | Yes | File repositories allow users to share files through external services and therefore can be disabled
Disable Link Shares | Yes | Link shares allow users to share passwords and files with anonymous users via link and can be disabled
Enforce central security reports  | Yes | Enforces that all security reports have to be sent to the server
Recurring security reports  | Yes | Allows administrators to specify an interval in which they want to tell users to generate new security reports


\* LDAP comes with some requirements, e.g. that the server needs to be able to provide the users plaintext password to the LDAP server or that the
server needs a way to re-encrypt the users secrets when the user is changing his password on the LDAP server.
Using this feature therefore necessarily will tell the client to send the plaintext password. (A warning is displayed to
the user before that happens, to prevent any misuse of that feature.) and the server will store the users secrets (not
his password) encrypted in the database to be able to re-encrypt them if necessary.

\** SAML & OIDC come with some requirements, e.g. that the server needs a way to to access the users secrets in order to be able
to provide the client with a random "fake password" that it then can use to decrypt the normal secrets.

::: tip
If you are interested in a feature that is not listed, then let us know.
:::


