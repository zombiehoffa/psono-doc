---
title: Browser Extension
metaTitle: Configure Psono's Browser Extension | Psono Documentation
meta:
- name: description
  content: Configuration guide for the Psono browser extensions for Chrome, Edge or Firefox.
---

# Configure Browser Extension

A small guide explaining how browser extensions can be centrally configured.

## Configuration (manually)

There are two ways to configure an extension. Either remote as explained in the next paragraph or manually, usually done by each user.

A user can manually configure the extension by filling in the right server address and then clicking on `Remote Config`

![Manual configuration Extension](/images/admin/install_browser_extension/manual_extension_configuration.png)

The extension will connect to the server, ask for the `WEBCLIENT_URL` and then connect to the webclient and download the `config.json`.



## Configuration (with config files)

As an alternative to the previous manual configuration you can also configure Psono extensions with config files. Depending
on the used browser and operation system these instructions differ.

## Windows

If you are in an Windows enterprise environment you can preconfigure extensions with entries in the registry, usually
rolled out by group policies. The next couple of paragraphs show how to configure Chrome, Firefox and Edge through
registry entries and provide some hints how to roll them out through GPOs. We are providing a dedicated guide for Microsoft's Intune here: [doc.psono.com/admin/configuration/intune.html](/admin/configuration/intune.html)



### Chrome

#### Automated Deployment

For instructions how to roll out extensions with Group Policies and Chrome you can find some decent instructions here:

[https://support.google.com/chrome/a/answer/7532015?hl=en](https://support.google.com/chrome/a/answer/7532015?hl=en)

The extension ID that you need is `eljmjmgjkbmpmfljlmklcfineebidmlo`

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create a key in your users registry called "ConfigJson" in:

```
HKLM\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy
```
as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_chrome.jpg)

The value of this key, is your `config.json` that you created previously for the webclient (with some escaped chars).

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy]
"ConfigJson"="{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
```

### Firefox

#### Automated Deployment

[https://support.mozilla.org/en-US/kb/customizing-firefox-using-group-policy-windows](https://support.mozilla.org/en-US/kb/customizing-firefox-using-group-policy-windows)

The extension ID that you need is `{3dce78ca-2a07-4017-9111-998d4f826625}`. (The brackets are part of the ID)

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create the following key `HKEY_LOCAL_MACHINE\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}` with a Default Value being a path pointing to a json file as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_firefox.jpg)

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}]
@="C:\\Program Files (x86)\\Mozilla Firefox\\browser\\extensions\\3dce78ca-2a07-4017-9111-998d4f826625.json"
```

Under `C:\Program Files (x86)\Mozilla Firefox\browser\extensions` you create a `3dce78ca-2a07-4017-9111-998d4f826625.json` file with this content:

```json
{
    "name": "{3dce78ca-2a07-4017-9111-998d4f826625}",
    "description": "ignored",
    "type": "storage",
    "data": {
        "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).


### Edge (based on Chromium)

#### Automated Deployment

For instructions how to roll out extensions with Group Policies and Edge you can find some decent instructions here:

[https://docs.microsoft.com/en-us/DeployEdge/deploy-edge-plan-deployment](https://docs.microsoft.com/en-us/DeployEdge/deploy-edge-plan-deployment)

The extension ID that you need is `eljmjmgjkbmpmfljlmklcfineebidmlo`

#### Pre-configuration:

In order to preconfigure the extension to point to your server, you have to create a key in your users registry called "ConfigJson" in:

```
HKLM\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy
```

as shown on this image:

![Registry](/images/admin/install_browser_extension/registry_edge.jpg)

The value of this key, is your `config.json` that you created previously for the webclient (with some escaped chars).

To automate this, you can put the following content into a psono.reg and execute it:

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy]
"ConfigJson"="{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
```

## Linux

If you are in an enterprise environment with a lot of linux clients, you can preconfigure the extension with your server with the following instructions.
Please take note taht the actual distribution depends highly on your infrastructure and as such is not covered by this documentation.

### Chrome

#### Pre-configuration:

For chrome create a `psono.json` in `/etc/opt/chrome/policies/managed` with the following content:

```json
{
    "3rdparty": {
        "extensions": {
            "eljmjmgjkbmpmfljlmklcfineebidmlo": {
                "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
            }
        }
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).

### Firefox

#### Pre-configuration:

For chrome create a `{3dce78ca-2a07-4017-9111-998d4f826625}.json` in `/usr/lib/mozilla/managed-storage` with the following content:

```json
{
    "name": "{3dce78ca-2a07-4017-9111-998d4f826625}",
    "description": "ignored",
    "type": "storage",
    "data": {
        "ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"
    }
}
```

The value of the "ConfigJson" property is your `config.json` that you created previously for the webclient (with some escaped chars).

