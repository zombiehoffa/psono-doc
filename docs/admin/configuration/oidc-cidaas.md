---
title: OIDC - cidaas
metaTitle: cidaas as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of cidaas as OIDC IDP
---

# cidaas as IDP for OIDC-SSO

To set up the IDP you need a running instance of cidaas.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure [cidaas](https://www.cidaas.com/) as OIDC-IDP for SSO. We assume that:

* your cidaas instance is running on https://cidaas.test.de
* your webclient can be accessed on https://psono.test.de
* the server is reachable at https://psono.test.de/server (e.g. https://psono.test.de/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## cidaas

1. Create a new app

    ![cidaas create app](/images/admin/configuration/oidc_cidaas1.png)

2. Configure app

    Configure the scopes and redirect urls as shown below.

    ![cidaas configure app](/images/admin/configuration/oidc_cidaas2.png)

## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach cidaas and vise versa.

Edit the settings.yml like so:

```yml
# Make sure the 'OIDC'-entry is present in the following list
AUTHENTICATION_METHODS: ['OIDC']

OIDC_CONFIGURATIONS:
    1:
        OIDC_RP_SIGN_ALGO: 'RS256'
        OIDC_RP_CLIENT_ID: 'XXXX'
        OIDC_RP_CLIENT_SECRET: 'XXXX'
        OIDC_OP_JWKS_ENDPOINT: 'https://test.cidaas.de/.well-known/jwks.json'
        OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://test.cidaas.de/authz-srv/authz'
        OIDC_OP_TOKEN_ENDPOINT: 'https://test.cidaas.de/token-srv/token'
        OIDC_OP_USER_ENDPOINT: 'https://test.cidaas.de/users-srv/userinfo'
        OIDC_USERNAME_ATTRIBUTE: 'email'
        OIDC_GROUPS_ATTRIBUTE_DICT_GROUP_ID: 'groupId'
```

The cidaas endpoints can be found on the ".well-known"-page (e.g. https://cidaas.test.de/.well-known/openid-configuration) of your installation

::: tip
Always restart the server after making changes in the `setting.yml`-file.
:::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

Update your config.json similar to the one shown below.

```json
{
  ...
    "authentication_methods": ["OIDC"],
    "oidc_provider": [{
      "title": "OIDC Login",
      "provider_id": 1,
      "button_name": "Login "
    }]
  ...
}
```

The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
to match the one that you used on your server.
