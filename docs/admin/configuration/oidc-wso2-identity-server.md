---
title: OIDC - WSO2 Identity Server
metaTitle: WSO2 Identity Server as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of WSO2 Identity Server as OIDC IDP
---

# WSO2 Identity Server as IDP for OIDC-SSO

To set up the IDP you need a running instance of WSO2 Identity Server with a configurable realm.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure WSO2 Identity Server as OIDC-IDP for SSO. We assume that:

* your WSO2 Identity Server instance is running on https://wso2.example.com,
* your webclient can be accessed on https://psono.example.com
* the server is reachable at https://psono.example.com/server (e.g. https://psono.example.com/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## WSO2 Identity Server

1. Navigate to Service Providers and click on `Add`.
2. Enter a nice name and description

    ![Name and describe Service provider](/images/admin/configuration/oidc_wso2_screen01.png)

3. Configure claims as shown below

    ![Name and describe Service provider](/images/admin/configuration/oidc_wso2_screen02.png)

4. Configure OpenID Connect

    ![Click on Configure](/images/admin/configuration/oidc_wso2_screen03.png)

4. Adjust config

    Enter `https://psono.example.com/server/oidc/1/callback/` as your callback url. Adjust the domain according to your setup.
    
    ![Click on Configure](/images/admin/configuration/oidc_wso2_screen04.png)

5. Copy `OAuth Client Key` and `OAuth Client Secret`

    We will need these two parameters later.

    ![Copy WSO2 Client key and secret](/images/admin/configuration/oidc_wso2_screen05.png)


## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach WSO2 Identity Server and vise versa.

Edit the settings.yml like so:

```yml
# Make sure the 'OIDC'-entry is present in the following list
AUTHENTICATION_METHODS: ['OIDC']

OIDC_CONFIGURATIONS:
    1:
        OIDC_RP_SIGN_ALGO: 'RS256'
        OIDC_RP_CLIENT_ID: '9ydFzCGsjR5_fkaWuBij3S8OfWga'
        OIDC_RP_CLIENT_SECRET: 'Z_1hBkB7QCU71XiPhfcI7bzTCksa'
        OIDC_USERNAME_ATTRIBUTE: 'sub'
        OIDC_OP_JWKS_ENDPOINT: 'https://wso2.example.com/oauth2/jwks'
        OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://wso2.example.com/oauth2/authorize'
        OIDC_OP_TOKEN_ENDPOINT: 'https://wso2.example.com/oauth2/token'
        OIDC_OP_USER_ENDPOINT: 'https://wso2.example.com/oauth2/userinfo?schema=openid'
```

- Replace `OIDC_RP_CLIENT_ID` and `OIDC_RP_CLIENT_SECRET` with the Client key and secret
- Replace the domain of all `OIDC_OP_...`-parameters to match your domain


::: tip
Always restart the server after making changes in the `setting.yml`-file.
:::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

Update your config.json similar to the one shown below.

```json
{
  ...
    "authentication_methods": ["OIDC"],
    "oidc_provider": [{
      "title": "OIDC Login",
      "provider_id": 1,
      "button_name": "Login "
    }]
  ...
}
```

The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
to match the one that you used on your server.
