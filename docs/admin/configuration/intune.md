---
title: Intune
metaTitle: Microsoft Intune | Psono Documentation
meta:
- name: description
  content: Installation and configuration guide for Psono's browser extension with Microsoft's Intune
---

# Intune

A brief guide how to distribute the browser extension centrally with Microsoft Intune (Endpoint Manager)

## Installation Windows

This guide explains how to distribute Psono in your company and install it for your users in Edge.

1.  Login to endpoint.microsoft.com

2.  Click on Devices

    ![Click on Devices](/images/admin/configuration/intune_install_1.png)

3.  Go to Windows devices > Configuration profiles

    ![Intune](/images/admin/configuration/intune_install_2.png)

4.  Click on Create profile

    ![Intune](/images/admin/configuration/intune_install_3.png)

5.  Basics

    Select" Windows 10 and later", "Templates" and "Administrative Templates"

    ![Intune](/images/admin/configuration/intune_install_4.png)

6.  Configuration settings

    Choose "Computer Configuration and" select "Control which extensions are installed silently"

    ![Intune](/images/admin/configuration/intune_install_5.png)

7.  Toggle Enabled

    Toggle Enabled and enter the the following value `eljmjmgjkbmpmfljlmklcfineebidmlo;https://clients2.google.com/service/update2/crx`

    ![Intune](/images/admin/configuration/intune_install_6.png)

8.  Assignments

    Assign the profile to the intended users / groups or devices as needed.

    ![Intune](/images/admin/configuration/intune_install_7.png)

9.  Review and create

    ![Intune](/images/admin/configuration/intune_install_8.png)

    It takes a while till the profile is applied to all users.

## Configuration Windows

To configure Psono's browser extension we will distribute powershell scripts that will do the necessary adjustments.

1.  Login to endpoint.microsoft.com

2.  Click on Devices

    ![Intune](/images/admin/configuration/intune_install_2.png)

3.  Go to Scripts

    ![Intune](/images/admin/configuration/intune_config_1.png)

4.  Click "Add" and select "Windows 10 and later"

    ![Intune](/images/admin/configuration/intune_config_2.png)

5.  Enter a meaningful title

    ![Intune](/images/admin/configuration/intune_config_3.png)

6.  Script settings

    Pick one of the PowerShell scripts below and check "Run script in 64 bit PowerShell Host"

    ![Intune](/images/admin/configuration/intune_config_4.png)

    - PowerShell script for Chrome:

    ```powershell
    if((Test-Path -LiteralPath "HKLM:\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy") -ne $true) {  New-Item "HKLM:\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy" -force -ea SilentlyContinue };
    New-ItemProperty -LiteralPath 'HKLM:\SOFTWARE\Policies\Google\Chrome\3rdparty\extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy' -Name 'ConfigJson' -Value '{"backend_servers":[{"title":"Your Company","url":"https://example.com/server"}],"base_url":"https://example.com/","allow_custom_server":false,"allow_registration":true,"allow_lost_password":true}' -PropertyType String -Force -ea SilentlyContinue;
    ```

    - PowerShell script for Edge:

    ```powershell
    if((Test-Path -LiteralPath "HKLM:\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy") -ne $true) {  New-Item "HKLM:\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy" -force -ea SilentlyContinue };
    New-ItemProperty -LiteralPath 'HKLM:\SOFTWARE\Policies\Microsoft\Edge\3rdparty\Extensions\eljmjmgjkbmpmfljlmklcfineebidmlo\policy' -Name 'ConfigJson' -Value '{"backend_servers":[{"title":"Your Company","url":"https://example.com/server"}],"base_url":"https://example.com/","allow_custom_server":false,"allow_registration":true,"allow_lost_password":true}' -PropertyType String -Force -ea SilentlyContinue;
    ```

    - PowerShell script for Firefox:

    ```powershell
    if((Test-Path -LiteralPath "HKLM:\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}") -ne $true) {  New-Item "HKLM:\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}" -force -ea SilentlyContinue };
    New-ItemProperty -LiteralPath 'HKLM:\SOFTWARE\Mozilla\ManagedStorage\{3dce78ca-2a07-4017-9111-998d4f826625}' -Name '(default)' -Value 'C:\Program Files\Mozilla Firefox\browser\extensions\3dce78ca-2a07-4017-9111-998d4f826625.json' -PropertyType String -Force -ea SilentlyContinue;
    New-Item 'C:\Program Files\Mozilla Firefox\browser\extensions' -ItemType Directory
    New-Item 'C:\Program Files\Mozilla Firefox\browser\extensions\3dce78ca-2a07-4017-9111-998d4f826625.json' -ItemType File -Value '{"name": "{3dce78ca-2a07-4017-9111-998d4f826625}","description": "ignored","type": "storage","data": {"ConfigJson": "{\"backend_servers\":[{\"title\":\"Your Company\",\"url\":\"https://example.com/server\"}],\"base_url\":\"https://example.com/\",\"allow_custom_server\":false,\"allow_registration\":true,\"allow_lost_password\":true}"}}' -Force
    ```

7.  Assignments

    Assign the script to the corresponding users / groups / devices.

8.  Review

    Review your settings and confirm.