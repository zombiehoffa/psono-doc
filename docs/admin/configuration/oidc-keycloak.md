---
title: OIDC - Keycloak
metaTitle: Keycloak as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Keycloak as OIDC IDP
---

# Keycloak as IDP for OIDC-SSO

To set up the IDP you need a running instance of Keycloak with a configurable realm.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure KeyCloak as OIDC-IDP for SSO. We assume that:

* your Keycloak instance is running on https://keycloak.example.com,
* you have a configured a realm named "psono",
* your webclient can be accessed on https://psono.example.com
* the server is reachable at https://psono.example.com/server (e.g. https://psono.example.com/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Keycloak

1. Navigate to your realm and click on `Create` in the `Clients`-Section.
2. In there add your new client like shown below

    ![client-create](/images/admin/configuration/oidc_keycloak_screen01.png)

3. Afterwards adapt the settings of the client. Don't forget to click on save at the bottom of the page.

    1. Give it a name and a short description if you wish.
    2. Ensure "Client Protocol" is set to "openid-connect"
    3. Change "Access Type" to "confidential"
    4. Verify that "Valid Redirect URIs" contains the correct value (ending with the wildcard "*")

    ![client-config](/images/admin/configuration/oidc_keycloak_screen02.png)

4. Switch to the "credentials" tab and note the "Secret" which will be used in the configuration of Psono later.

5. Create group mapper

    Go to Mappers and create one configured as shown below
    
    ![keycloak create group mapper](/images/admin/configuration/oidc_keycloak_create_mapper.png)
    

## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach Keycloak and vise versa.

Edit the settings.yml like so:

```yml
# Make sure the 'OIDC'-entry is present in the following list
AUTHENTICATION_METHODS: ['OIDC']

OIDC_CONFIGURATIONS:
    1:
        OIDC_RP_SIGN_ALGO: 'RS256'
        OIDC_RP_CLIENT_ID: 'psono'
        OIDC_RP_CLIENT_SECRET: 'd61d048a-100e-4299-885f-b3e23539b60c'
        OIDC_OP_JWKS_ENDPOINT: 'https://keycloak.example.com/auth/realms/psono/protocol/openid-connect/certs'
        OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://keycloak.example.com/auth/realms/psono/protocol/openid-connect/auth'
        OIDC_OP_TOKEN_ENDPOINT: 'https://keycloak.example.com/auth/realms/psono/protocol/openid-connect/token'
        OIDC_OP_USER_ENDPOINT: 'https://keycloak.example.com/auth/realms/psono/protocol/openid-connect/userinfo'
```

The keycloak endpoints can be found on the ".well-known"-page (e.g. https://keycloak.example.com/auth/realms/psono/.well-known/openid-configuration) of your installation

::: tip
Always restart the server after making changes in the `setting.yml`-file.
:::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

Update your config.json similar to the one shown below.

```json
{
  ...
    "authentication_methods": ["OIDC"],
    "oidc_provider": [{
      "title": "OIDC Login",
      "provider_id": 1,
      "button_name": "Login "
    }]
  ...
}
```

The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
to match the one that you used on your server.
