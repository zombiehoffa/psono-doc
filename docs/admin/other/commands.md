---
title: Commands
metaTitle: Commands | Psono Documentation
meta:
  - name: description
    content: An overview of the commands offered by the Psono server
---

# Commands

The Psono server provides multiple commands that can be either executed on the console or remote with REST calls.

[[toc]]

## Console commands

Each of the following commands can be executed locally. If you have installed Psono CE the command can be executed like:

```bash
docker run --rm \
  -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
  -ti psono/psono-combo:latest python3 ./psono/manage.py COMMAND ARG1 ARG2
```

In the Psono EE version it would be:

```bash
docker run --rm \
  -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
  -ti psono/psono-combo-enterprise:latest python3 ./psono/manage.py COMMAND ARG1 ARG2
```

## Remote commands

Psono provides the possibility to execute every command "remote", protected with a shared access key that needs to be passed
as authorizartion header.

Configure the shared access key in your `settings.yml` as

```yaml
MANAGEMENT_COMMAND_ACCESS_KEY: 'SOME_SUPER_SECRET_RANDOM_SECRET'
```
Restart the server afterwards.

::: warning
This access key allows everyone to execute management commands, so keep it secret!
:::

Every management command can be executed now remote e.g. with curl:

```bash
curl -d '{"command_name":"COMMAND", "command_args":["ARG1", "ARG2"]}' \
  -H "Authorization: Token SOME_SUPER_SECRET_RANDOM_SECRET" \
  -H "Content-Type: application/json" \
  -X POST https://example.com/server/management-command/
```


## List of commands

### cleartoken

Clears all expired tokens

	python3 ./psono/manage.py cleartoken

(usually executed in  a cronjob)


### createuser

Creates a user with a given username, password and email

	python3 ./psono/manage.py createuser username@example.com myPassword email@something.com

(usually executed by developers when they want to create a new user for testing)


### deleteuser

Deletes a user with a given username

	python3 ./psono/manage.py deleteuser username@example.com

(usually executed by developers when they want to cleanup after testing, or want to try again with a fresh user)


### disableinactiveusers

Disables users that have not logged in for the past x seconds (e.g. 2592000 for 30 days).

	python3 psono/manage.py disableinactiveusers 2592000

One can test the effect of the command with a dry run, e.g.

	python3 psono/manage.py disableinactiveusers --dry-run 2592000


### disableuser

Disables a user. The user won't be able to login anymore.

	python3 ./psono/manage.py disableuser username@example.com

### enableuser

Enables a user. The user will be able to login again.

	python3 ./psono/manage.py enableuser username@example.com

### fsclustercreate

Creates a cluster. Takes a title of the cluster as argument.

	python3 ./psono/manage.py fsclustercreate "Default Cluster"

### fsclusterdelete

Deletes a cluster. Takes the id of a cluster as argument

	python3 ./psono/manage.py fsclusterdelete 1713b68b-df64-41c5-822c-6eb8c877037c

### fsclusterlist

Lists all configured clusters

	python3 ./psono/manage.py fsclusterlist

### fsclustershowconfig

Creates the configuration of a potential cluster member for a cluster. Takes the id of a cluster as argument.

	python3 ./psono/manage.py fsclustershowconfig 1713b68b-df64-41c5-822c-6eb8c877037c

### fsshardcreate

Creates a shard with a title and a description as argument

	python3 ./psono/manage.py fsshardcreate "Some Title" "Some description"

(The title will be later visible to the user if multiple shards are available, so the user can pick one)

### fssharddelete

Deletes a shard. Takes the id of the shard that should be deleted as argument.

	python3 ./psono/manage.py fssharddelete a8a1176c-bad8-4b84-b45e-1084b3a48e7d

### fsshardlink

Links a shard and a cluster together, allowing fileservers that belong to a cluster to announce their availability to serve
a shard. Takes the cluster id and the shard id as arguments and in addition the permissions (e.g. rw for read and write)

	python3 ./psono/manage.py fsshardlink 1713b68b-df64-41c5-822c-6eb8c877037c a8a1176c-bad8-4b84-b45e-1084b3a48e7d rw

### fsshardlist

Lists all configured shards.

	python3 ./psono/manage.py fsshardlist

### fsshardunlink

Destroys the link between a shard and a cluster, preventing fileservers of this clusters to announce this shard.
Takes the ID of the link as argument.

	python3 ./psono/manage.py fsshardunlink c8916ab1-4a22-4aa6-9549-c0b15f688e7c

### promoteuser

Promotes a user and grants superuser privilege (or other roles)

	python3 ./psono/manage.py promoteuser username@example.com superuser

### demoteuser

Demotes a user and revokes superuser privilege (or other roles)

	python3 ./psono/manage.py demoteuser username@example.com superuser

### verifyuseremail

Marks the email of a user as verified

	python3 ./psono/manage.py verifyuseremail username@example.com

### generateserverkeys

Generates some new server keys for the settings.yaml

	python3 ./psono/manage.py generateserverkeys

(usually executed only once during the installation phase)

### presetup

If the database user has the necessary permission this command will install the necessary postgres extensions ltree and pgcrypto.

	python3 ./psono/manage.py presetup

(usually executed only once during the installation phase)


### sendtestmail

Sends a test email to the given email address.

	python3 ./psono/manage.py sendtestmail something@something.com

(usually executed only during installation, to test if the email configuration is correct)


### reset2fa

Removes all second factors for a given account

	python3 ./psono/manage.py reset2fa something@something.com

(usually used if a user loses his phone or yubikey and an administrator wants to reset his second factor)


### testduoconnection

Tests your DUO connection with a given integration key, secret key and host.

	python3 ./psono/manage.py testduoconnection DI... lt... api-XXXXXXX.duosecurity.com

(usually executed only during installation, to test if the system wide DUO configuration is correct)

You can also specify an additional username. This will test PUSH authentication with the user.

	python3 ./psono/manage.py testduoconnection DI... lt... api-XXXXXXX.duosecurity.com something@example.com

(In the example above the username in DUO would be "something", so everything before the "@".)


### syncldap

Syncs users (and their activity state) from LDAP connection (EE Version only). This command currently works only in
combination with a Windows AD.

	python3 ./psono/manage.py syncldap

::: warning
This command is potentially dangerous as it will logout users, deactivate them and potentially delete them. Before
executing this function for the first time use the `--dryrun` flag to display all the changes that the script will apply.
:::


### testldap

Tests your LDAP connection (EE Version only)

	python3 ./psono/manage.py testldap username@something.com thePassWord

(usually executed only during installation, to test if the LDAP configuration is correct)


### testldapgateway

Tests your LDAP gateway connection (EE Version only)

	python3 ./psono/manage.py testldapgateway username@something.com thePassWord

(usually executed only during installation, to test if the LDAP gateway configuration is correct)


### testsaml

Tests your SAML connection (EE Version only)

	python3 ./psono/manage.py testsaml

(usually executed only during installation, to test if the SAML configuration is correct)

