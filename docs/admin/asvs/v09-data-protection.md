---
title: V9 Data Protection
metaTitle: V9 Data Protection | Psono Documentation
meta:
  - name: description
    content: Data protection verification requirements
---

# V9 Data Protection

Data protection verification requirements

[[toc]]

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 9.1                         | Verify that all forms containing sensitive information have disabled client side caching, including autocomplete features.                                                                                                                                                                                                                                     | x                       | x          | x           | 1.0       |
| 9.2                         | Verify that the list of sensitive data processed by the application is identified, and that there is an explicit policy for how access to this data must be controlled, encrypted and enforced under relevant data protection directives.                                                                                                                      |                         |            | x           | 1.0       |
| 9.3                         | Verify that all sensitive data is sent to the server in the HTTP message body or headers (i.e., URL parameters are never used to send sensitive data).                                                                                                                                                                                                         | x                       | x          | x           | 1.0       |
| 9.4                         | Verify that the application sets appropriate anti-caching headers as per the risk of the application such as the following: <br> Expires: Tue, 03 Jul 2001 06:00:00 GMT <br>Last-Modified: {now} GMT<br>Cache-Control: no-store, no-cache, mustrevalidate, max-age=0<br>Cache-Control: post-check=0, pre-check=0<br>Pragma: no-cache                           | x                       | x          | x           | 1.0       |                                                                                                                                                                                                                                                                                                                                                         | x                       | x          | 1.0         |           |
| 9.5                         | Verify that on the server, all cached or temporary copies of sensitive data stored are protected from unauthorized access or purged/invalidated after the authorized user accesses the sensitive data.                                                                                                                                                         |                         | x          | x           | 1.0       |
| 9.6                         | Verify that there is a method to remove each type of sensitive data from the application at the end of the required retention policy.                                                                                                                                                                                                                          |                         |            | x           | 1.0       |
| 9.7                         | Verify the application minimizes the number of parameters in a request, such as hidden fields, Ajax variables, cookies and header values.                                                                                                                                                                                                                      |                         | x          | x           | 2.0       |
| 9.8                         | Verify the application has the ability to detect and alert on abnormal numbers of requests for data harvesting for an example screen scraping.                                                                                                                                                                                                                 |                         |            | x           | 2.0       |
| 9.9                         | Verify that data stored in client side storage (such as HTML5 local storage, session storage, IndexedDB, regular cookies or Flash cookies) does not contain sensitive data or PII.                                                                                                                                                                             | x                       | x          | x           | 3.0.1     |
| 9.10                        | Verify accessing sensitive data is logged, if the data is collected under relevant data protection directives or where logging of accesses is required.                                                                                                                                                                                                        |                         | x          | x           | 3.0       |
| 9.11                        | Verify that sensitive information maintained in memory is overwritten with zeros as soon as it no longer required, to mitigate memory dumping attacks.                                                                                                                                                                                                         |                         | x          | x           | 3.0.1     |



### 9.1

All forms in the client that contain for example the master password have been marked for the browser not to be cached.

### 9.2

All data that the application holds is sensitive.

One has to distinguish between sensitive data that the server has access to (e.g. user email addresses) and sensitive data
that the server has no access to.
- All sensitive data that the server has access to is stored encrypted in the database. Access is restricted to administrators
and only disclosed in order to protect the system or to inform the user about security related information (e.g. unusual activity)
- All sensitive data that the server has no access to is encrypted by the client before leaving the browser.
This data cannot be accessed by anyone besides the user who holds the master password.

### 9.3

URL parameters (besides UUIDs for data objects) are not used by Psono, so no sensitive information is ever part of any URL parameter.

### 9.4

Appropriate Headers are in place on psono.pw and part of the example configuration in the documentation.

### 9.5

No sensitive data is stored in a way that would allow administrators to use them.

### 9.6

No data is stored longer than needed. Session data (tokens) are destroyed after their expiration by a nightly cron job.

### 9.7

The API does not implement any unused or unnecessary fields.

### 9.8

Psono has rate limiting in place to minimize those attacks. All requests will produce as part of the audit logging feature
of the EE version an appropriate logging entry. Its up to the logging software to produce corresponding alerts.

### 9.9 (violation)

Even so Psono client does it best to only store the minimal amount needed in the local storage of the system, some
information is required to be stored locally. e.g. the keys to decrypt secrets. The master password is never stored in
any local storage. Some PII may be stored in your local storage (e.g. your users email address).

### 9.10

All access to any data is logged (for normal users and for superusers)

### 9.11 (violation)

Most probably not implemented by the used libraries.


