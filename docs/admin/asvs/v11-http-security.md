---
title: V11 HTTP security
metaTitle: V11 HTTP security | Psono Documentation
meta:
  - name: description
    content: HTTP security configuration verification requirements
---

# V11 HTTP security

HTTP security configuration verification requirements

[[toc]]

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 11.1                        | Verify that the application accepts only a defined set of required HTTP request methods, such as GET and POST are accepted, and unused methods (e.g. TRACE, PUT, and DELETE) are explicitly blocked.                                                                                                                                                           | x                       | x          | x           | 1.0       |
| 11.2                        | Verify that every HTTP response contains a content type header specifying a safe character set (e.g., UTF-8, ISO 8859-1).                                                                                                                                                                                                                                      | x                       | x          | x           | 1.0       |
| 11.3                        | Verify that HTTP headers added by a trusted proxy or SSO devices, such as a bearer token, are authenticated by the application.                                                                                                                                                                                                                                |                         | x          | x           | 2.0       |
| 11.4                        | Verify that a suitable X-FRAME-OPTIONS header is in use for sites where content should not be viewed in a 3rd-party X-Frame.                                                                                                                                                                                                                                   |                         | x          | x           | 3.0.1     |
| 11.5                        | Verify that the HTTP headers or any part of the HTTP response do not expose detailed version information of system components.                                                                                                                                                                                                                                 | x                       | x          | x           | 2.0       |
| 11.6                        | Verify that all API responses contain X-Content-Type-Options: nosniff and Content-Disposition: attachment; filename=""api.json"" (or other appropriate filename for the content type).                                                                                                                                                                         | x                       | x          | x           | 3.0       |
| 11.7                        | Verify that a content security policy (CSPv2) is in place that helps mitigate common DOM, XSS, JSON, and JavaScript injection vulnerabilities.                                                                                                                                                                                                                 | x                       | x          | x           | 3.0.1     |
| 11.8                        | Verify that the X-XSS-Protection: 1; mode=block header is in place.                                                                                                                                                                                                                                                                                            | x                       | x          | x           | 3.0       |

### 11.1

Each endpoint has explicitely defined, which HTTP methods are allowed. Unused methods throw a 405 (method not supported)

### 11.2 (violation)

Responses currently do not define a charset even so they all return UTF-8

### 11.3

There are no trusted proxies or SSO devices that would add headers dynamically.

### 11.4

The default configuration of the reverse proxy enforces X-Frame Option to be DENY. Cloudflare as default uses sameorigin which is fine too

### 11.5 (violation)

Cloudflare infront hides most of the fingerprint of the webserver. The server itself exposes his Psono version in order to enable the possibility for the client to check for incompatibilities.

### 11.6

X-Content Type option nosniff is set in the default configuration of the reverse proxy and also passed on by cloudflare

### 11.7

CSP is in place and the complete application is written so it can be enabled in a very strict mode. The CSP is also part of the default configuration of the reverse proxy

### 11.8

" X-XSS-Protection: 1; mode=block " is part of the default configuration of the reverse proxy and also by default enabled.

