---
title: 1b. Update Server EE
metaTitle: Update Psono server EE | Psono Documentation
meta:
  - name: description
    content: Update instruction for the Enterprise Edition of the Psono server
  - name: robots
    content: noindex, nofollow
---

# Update Psono server EE

Update instruction for the Enterprise Edition of the Psono server

## Preamble

From time to time you want to update to the latest version. At this point we assume that you have created a backup of
your postgres database. If you have a possibility to snapshot your machine, please do so.

## Update with Docker

1. Update the docker image

    ```bash
    docker pull psono/psono-server-enterprise:latest
    ```

2. Update the database

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server-enterprise:latest python3 ./psono/manage.py migrate
   ```

3. Stop old psono-server-enterprise

    ```bash
    docker stop psono-server-enterprise
    ```

4. Start new psono-server-enterprise

    ```bash
    docker run --name psono-server-enterprise-new \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /path/to/log/folder:/var/log/psono \
        -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest
    ```

    ::: tip
    If you used custom volume mounts e.g. for custom branding or a custom CA certificate for LDAPs, then make sure to adjust the command accordingly and add your volume mounts.
    :::

5. Cleanup

    If everything works you can cleanup your containers with the following commands:

    ```bash
    docker rm psono-server-enterprise
    docker rename psono-server-enterprise-new psono-server-enterprise
    ```

If anything fails you should be able to restore the database, and start the old docker container again.



