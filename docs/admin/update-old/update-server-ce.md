---
title: 1a. Update Server CE
metaTitle: Update Psono server CE | Psono Documentation
meta:
  - name: description
    content: Update instruction for the Community Edition of the Psono server
  - name: robots
    content: noindex, nofollow
---

# Update Psono server CE

Update instruction for the Community Edition of the Psono server

## Preamble

From time to time you want to update to the latest version. Dependent on the installation you will have to follow
different (but similar) procedures. At this point we assume that you have created a backup of your postgres database.
If you have a possibility to snapshot your machine, please do so. The guide contains some steps to backup the application
yet due to updating some system dependencies / python packages on the way can fail in some cases.

## Update with Docker

1. Update the docker image

        docker pull psono/psono-server:latest

2. Update the database

        docker run --rm \
          -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
          -ti psono/psono-server:latest python3 ./psono/manage.py migrate

3. Stop old psono-server

        docker stop psono-server

4. Start new psono-server

    ```bash
    docker run --name psono-server-new \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -d --restart=unless-stopped -p 10100:80 psono/psono-server:latest
    ```

    ::: tip
    If you used custom volume mounts e.g. for custom branding, then make sure to adjust the command accordingly and add your volume mounts.
    :::

4.  Cleanup

	If everything works you can cleanup your containers with the following commands:

		docker rm psono-server
		docker rename psono-server-new psono-server

If anything fails you should be able to restore the database, and start the old docker container again.

## Update for Ubuntu

This guide will update the Psono server. It has been tested on Ubuntu 18.04.

1.  Become the psono user

    ```bash
    sudo -iu psono
    ```

2.  Backup the git folder

    ```bash
    mv ~/psono-server ~/psono-server-backup
    ```

3.  Clone the updated newest version

    ```bash
    git clone https://gitlab.com/psono/psono-server.git ~/psono-server
    ```

4.  Become root

    ```bash
    Ctrl + D
    sudo -i
    ```

5.  Install python requirements

    ```bash
    cd /home/psono/psono-server
    pip3 install -r requirements.txt
    su psono                            # become psono again
    ```

6.  Update your database

    ```bash
    python3  ~/psono-server/psono/manage.py migrate
    ```

7.  Restart the Psono server

    ```bash
    supervisorctl restart psono-server
    ```


8.  Cleanup

	If everything works you can remove the backup folder

    ```bash
    sudo rm /home/psono/psono-server-backup
    ```


If anything fails you should be able to restore the database, delete the new psono-server folder and copy the backup
folder back to its original place.

## Update for CentOS

This guide will update the Psono server. It has been tested on CentOS 7.

1.  Become the psono user

    ```bash
    sudo -iu psono
    ```

2.  Backup the git folder

    ```bash
    mv ~/psono-server ~/psono-server-backup
    ```

3.  Clone the updated newest version

    ```bash
    git clone https://gitlab.com/psono/psono-server.git ~/psono-server
    ```

4.  Become root

    ```bash
    Ctrl + D
    sudo -i
    ```

5.  Install python requirements

    ```bash
    cd /home/psono/psono-server
    pip3 install -r requirements.txt
    su psono                            # become psono again
    ```

6.  Update your database

    ```bash
    python3  ~/psono-server/psono/manage.py migrate
    ```

7.  Restart the Psono server

    ```bash
    supervisorctl restart psono-server
    ```

8.  Cleanup

	If everything works you can remove the backup folder

    ```bash
    sudo rm /home/psono/psono-server-backup
    ```

If anything fails you should be able to restore the database, delete the new psono-server folder and copy the backup
folder back to its original place.
