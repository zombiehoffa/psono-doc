---
title: 0. Installation Preparation
metaTitle: Installation Preparation | Psono Documentation
meta:
  - name: description
    content: How to prepare for a production grade installation Psono
---

# Installation Preparation

This section will describe how to prepare for a production grade installation of Psono.

## System Requirements

A production grade setup of Psono contains

- 1 VM (or server) for the database
- 1 VM (or server) for the server and client module and admin portal
- 1 VM (or server) for the fileserver module (if you plan to use it)
- 1 VM (or server) for the redis cache (if you plan to setup HA)

A database should always be separated from the application itself, which allows restoration of backups with no data loss
and offers later the possibility to easier scale if necessary.
The fileserver (if you plan to use it) should also be installed on a separate machine to avoid friction due to low
storage or a low amount of free bandwidth or higher disk usage.
If you have enough users, you want to have a High Availability Setup to avoid downtimes for which you require a central
cache to share the state.

In general you should size your installation according to the expected usage and therefore amount of users. Here is a
small overview.

|   | < 100 users | < 500 | < 1000 (HA) | > 1000 (HA) |
|---|-------------|-------|--------|--------|
| Database | 1 VM with 1 CPU, 1.5 GB RAM, 20 GB HDD | 1 VM with 2 CPUs, 3 GB RAM, 20 GB HDD | 1 Server with 4 CPUs, 6 GB RAM, 30 GB SSD | 1 Server with 8 CPUs, 12 GB RAM, 60 GB SSD |
| Server  | 1 VM with 1 CPU, 1 GB RAM, 10 GB HDD | 1 VM with 2 CPUs, 2 GB RAM, 10 GB HDD | 2 VM (for HA) with 4 CPUs, 4 GB RAM, 10 GB HDD | 2 VMs (for HA) with 4 CPUs, 4 GB RAM, 10 GB HDD |
| Fileserver | 1 VM with 1 CPU, 1 GB RAM, XXX GB HDD | 1 Server with 2 CPUs, 2 GB RAM, XXX GB HDD | 1 Server with 4 CPUs, 4 GB RAM, XXX GB HDD | - |
| Redis Cache | (not necessary) | (not necessary) | 1 VM with 2 CPUs, 2 GB RAM | 1 VM with 2 CPUs, 4 GB RAM |

::: tip
These estimates are only a rough estimate and are heavily influenced by your users actual usage of the system.
:::


- Database: Disk consumption is extremely small, yet keep in mind that you might want to do backups and need some extra storage to do the postgres dump.
- Server, client and admin portal: Disk consumption is not extensive. You only need enough to store all the Docker images.
- Fileserver: The fileserver storage depends on the files that you want to store. If users only store very sensitive data, then a few GB per user are enough.
If you plan to upload your nightly backups there then a few more GB might be needed. With an higher amount of users, you should also think about how to shard data.
Psono supports multiple concepts like Active Active replication and site affinity to distribute the load and bandwdith across multiple locations and offices if required.
Check out the [install guide for the fileserver](/admin/installation/install-fileserver.html) for more details.



## Software Requirements

- Docker
- Postgres 13 (but preferable latest)
- Either Chrome / Firefox / Chromium / Edge (Chromium) or Brave

## Other Requirements

- Domain (or subdomain) pointing to your system
- Trusted SSL certificate (or the knowledge / infrastructure to setup letsencrypt)

::: warning
A domain and SSL are hard requirements, Setups with IPs, http or not public trusted certificates are not supported.
:::