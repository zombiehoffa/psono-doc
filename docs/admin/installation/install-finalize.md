---
title: 4. Finalize
metaTitle: Install final step | Psono Documentation
meta:
  - name: description
    content: The last step explaining how to create the first user and how to login
---

# Finalize

This guide explains the final steps, how to create the first user and the urls to login.

## Preamble

We assume that 

* The command `docker ps` shows running healthy docker containers
* When you open `https://psono.example.com` you see the regular webinterface of Psono
* When you open `https://psono.example.com/server/healthcheck/` reports all healthy

## Create first user

Now after setting up Psono, you can create your first user. If you configured mail you can use the regular registration
procedure. If not you can use the following command to create a user on the commandline:

1.  Create user

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py createuser username@example.com myPassword email@something.com
    ```
    
    ::: tip
    Usernames in Psono must follow the pattern of an email address. The domain part should usually match the one that is shown
    on the login screen and should be part of the `ALLOWED_DOMAINS` in the `settings.yaml`
    :::

2.  Test

    Test your login on `https://psono.example.com`. This is the webinterface that regular users will use to access their
    vault and create their entries.

## Promote admin

Now that we have a user we can promote that user so that we can login to the admin portal. 

1.  Promote user

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py promoteuser username@example.com superuser
    ```

    ::: tip
    Only the first admin needs to be promoted from the command line. Afterwards you can use the admin portal.
    :::

2.  Test

    Test your login on `https://psono.example.com/portal/`. This is the webinterface or portal that only admins will use.