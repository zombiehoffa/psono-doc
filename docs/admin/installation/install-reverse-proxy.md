---
title: 3. Install Reverse Proxy
metaTitle: Install Psono Reverse Proxy | Psono Documentation
meta:
  - name: description
    content: The Psono usually requires a reverse proxy, to handle TLS. This section will explain how to install one of those reverse proxies.
---

# Install Reverse Proxy

The Psono usually requires a reverse proxy, to handle TLS. This section will explain how to install one of those reverse proxies.

## Preamble

This whole guide is based on Ubuntu 22.04 LTS. Other Ubuntu and Debian based systems should be similar if not even identical.

We assume that you want to use:

* https://psono.example.com to host Psono
* you have a valid certificate in /etc/ssl/ with fullchain.pem and privkey.pem
* An A-Record for psono.example.com exists, pointing to your server's ip address
* The docker container runs on port 10200

## Reverse Proxy with Nginx

1.  Install Nginx

    ```bash
    sudo apt-get install nginx
    ```

2.  Create nginx config

	Create `psono.example.com.conf` in `/etc/nginx/sites-available` with the following content:

    ```
    server {
        listen 80;
        server_name psono.example.com;
        return 301 https://$host$request_uri;
    }

    server {
        listen 443 ssl http2;
        server_name psono.example.com;

        ssl_protocols TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_session_cache shared:SSL:10m;
        ssl_session_tickets off;
        ssl_stapling on;
        ssl_stapling_verify on;
        ssl_session_timeout 1d;
        resolver 8.8.8.8 8.8.4.4 valid=300s;
        resolver_timeout 5s;
        ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';

        # Comment this in if you know what you are doing
        # add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";

        add_header Referrer-Policy same-origin;
        add_header X-Frame-Options DENY;
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";

        # If you have the admin fileserver installed too behind this reverse proxy domain, add your fileserver URL e.g. https://fs01.example.com as connect-src too:
        add_header Content-Security-Policy "default-src 'none';  manifest-src 'self'; connect-src 'self' https://static.psono.com https://api.pwnedpasswords.com https://storage.googleapis.com https://*.digitaloceanspaces.com https://*.blob.core.windows.net https://*.s3.amazonaws.com; font-src 'self'; img-src 'self' data:; script-src 'self'; style-src 'self' 'unsafe-inline'; object-src 'self'; child-src 'self'";

        ssl_certificate /etc/ssl/fullchain.pem;
        ssl_certificate_key /etc/ssl/privkey.pem;

        client_max_body_size 256m;

        gzip on;
        gzip_disable "msie6";

        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_min_length 256;
        gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

        root /var/www/html;

        location ~* \.(?:ico|css|js|gif|jpe?g|png|eot|woff|woff2|ttf|svg|otf)$ {
            expires 30d;
            add_header Pragma public;
            add_header Cache-Control "public";

            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;
            proxy_hide_header Content-Security-Policy;
            
            proxy_pass          http://localhost:10200;
            proxy_redirect      http://localhost:10200 https://psono.example.com;
        }


        location / {
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;
            proxy_hide_header Content-Security-Policy;
		
            proxy_pass          http://localhost:10200;
            proxy_read_timeout  90;
		
            proxy_redirect      http://localhost:10200 https://psono.example.com;
        }
    }
    ```


3.  Enable nginx config

	```bash
	ln -s /etc/nginx/sites-available/psono.example.com.conf /etc/nginx/sites-enabled/
	```

3.  Test nginx config

	```bash
	sudo nginx -t
	```

4.  Restart nginx

	```bash
	sudo service nginx restart
	```

    You should now be able to open https://psono.example.com in your browser and see the Psono webclient which is used 
    by a regular user.
    If you open https://psono.example.com/server/info/ you should see the following:

	```
	{"info":"{\"version\": \"....}
	```

    The url https://psono.example.com/portal/ should show login screen of the admin portal which administrators will use
    to administrate certain aspects of Psono.
