---
home: true
heroImage: /images/hero.png
actionText: Get Started →
actionLink: /user/getting-started/overview.html
footer: Apache 2.0 Licensed | Copyright © 2020 Psono
---

<div class="features">
  <div class="feature">
    <h2>Users</h2>
    <p>You lost your passwords, don't know how to use a specific feature? Then you can find all you need to know <a href="/user/getting-started/overview.html">here</a>.</p>
  </div>
  <div class="feature">
    <h2>Admins</h2>
    <p>Admins are usually more interested in how to install and operate Psono. The necessary instructions can be found <a href="/admin/overview/summary.html">here</a>.</p>
  </div>
  <div class="feature">
    <h2>Developers</h2>
    <p>If you are a developer and looking to contribute to Psono you will find the necessary instructions to kickstart your adventure <a href="/admin/development/contribution-agreement.html">here</a>.</p>
  </div>
</div>

### Quickstart for Users

Check out [Psono.pw](https://www.psono.pw/). Our hosted solution free of charge meant for demo purposes.


### Quickstart for Admins

If you want to install an own server check out our [Quickstart Script](https://gitlab.com/psono/psono-quickstart). It will install Psono in a matter of minutes on your own server.

1) Point a DNS record to your server

This DNS record will be something that you later use to access the installation. Usually a subdomain psono.example.com

2) Execute the following

``` bash
bash <(curl -s https://gitlab.com/psono/psono-quickstart/raw/master/install.sh)
```

More infos can be found [here](https://gitlab.com/psono/psono-quickstart)

::: warning
This script is only meant for demo purposes and not for a production grade system and meant to be run on Debian, Ubuntu or SUSE.
:::

